#include<stdio.h>
#include<string.h>
#include<pthread.h>
#include<stdlib.h>
#include<unistd.h>
#include<sys/types.h>
#include<sys/wait.h>
#include <dirent.h>

pthread_t tid[4], tid2[4], tid3[4]; //inisialisasi array untuk menampung thread 
pid_t child;

void download() {
	pid_t child_id;
 	int status;

  char *url[] = {
    "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt",
    "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1"
  	};
	char *file[] = {"quote.zip", "music.zip"};
	
  for(int i = 0; i < 2; i++) {
  char *argv[] = {"wget", "-q", "--no-check-certificate", url[i], "-O", file[i], NULL};
    if((child_id = fork()) == 0) {
      execv("/usr/bin/wget", argv);
    }

    while(wait(&status) > 0);
	}
}

void* unzip(void *arg)
{
	int status;
	char *argv1[] = {"mkdir", "-p", "/home/natya/Modul3/quote", NULL};
	char *argv2[] = {"mkdir", "-p", "/home/natya/Modul3/music", NULL};
	char *argv3[] = {"unzip","-q" ,"quote.zip","-d","/home/natya/Modul3/quote", NULL};
	char *argv4[] = {"unzip","-q" ,"music.zip","-d","/home/natya/Modul3/music", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/mkdir", argv1);
	    	}
	   while(wait(&status) > 0);
	}
	else if(pthread_equal(id,tid[1])) 
	{
       child = fork();
        if (child==0) {
		    execv("/usr/bin/mkdir", argv2);
	    }
	   while(wait(&status) > 0);
	}
	else if(pthread_equal(id,tid[2])) 
	{
       child = fork();
        if (child==0) {
		    execv("/usr/bin/unzip", argv3);
	    }
	   while(wait(&status) > 0);
	}
	else if(pthread_equal(id,tid[3])) 
	{
       child = fork();
        if (child==0) {
		    execv("/usr/bin/unzip", argv4);
	    }
	    while(wait(&status) > 0);
	}
	
	return NULL;
}

void zip(){
	int status;
	char *argv[] = {"zip", "-P", "mihinomenestnatya", "-qr", "hasil.zip", "hasil", NULL};
	child = fork();
		if (child==0) {
		    execv("/usr/bin/zip", argv);
	    	}
	   while(wait(&status) > 0);
}

//untuk decode 
char base46[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* c) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(c) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; c[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46[k] != c[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}

void listFilesRecursively(char *basePath, char *hasilPath)
{
    char path[100];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

 				FILE *fp = fopen(path, "r");
 				
 				const unsigned MAX_LENGTH = 256;
    			char buffer[MAX_LENGTH];

    			while (fgets(buffer, MAX_LENGTH, fp)){
    				FILE *hasil = fopen(hasilPath,"a");
    				fprintf(hasil,"%s\n", base64_decode(buffer));
    				fclose(hasil);
    			}
        		
				 // close the file
				 fclose(fp);
       
    }

    closedir(dir);
}

void* decode(void *arg){
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid2[0]))
	{
		listFilesRecursively("/home/natya/Modul3/music", "/home/natya/Modul3/hasil/music.txt");
	}
	else if(pthread_equal(id,tid2[1])) 
	{
       listFilesRecursively("/home/natya/Modul3/quote", "/home/natya/Modul3/hasil/quote.txt");
	}
}

void makedir(char *path){
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/usr/bin/mkdir", argv);
	} else {
		((wait(&status)) > 0);
	}
}

void remove_file(){
	int status;
	char *argv[] = {"rm", "-r", "/home/natya/Modul3/hasil", NULL};
	child = fork();
	if (child==0) {
		execv("/usr/bin/rm", argv);
	}
	while(wait(&status) > 0);
}

void unzip2(){
	   
	int status;
	char *argv[] = {"unzip", "-P", "mihinomenestnatya", "-q", "/home/natya/Modul3/hasil.zip", NULL};

	child = fork();
	if (child==0) {
	    execv("/usr/bin/unzip", argv);
	}
	while(wait(&status) > 0);
	
}

int main(void)
{
	int i=0, j=0;
	int err,err2,err3;
	
	download();
	
	//unzip
	while(i < 4) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&unzip,NULL); //membuat thread
		i++;
	}
	
	while(j < 4){
		pthread_join(tid[j],NULL);
		j++;
	}
	
	i=0, j=0;
	makedir("/home/natya/Modul3/hasil");
	//decode
	while(i < 3) // loop sejumlah thread
	{
		err2=pthread_create(&(tid2[i]),NULL,&decode,NULL); //membuat thread
		i++;
	}
	
	while(j < 3){
		pthread_join(tid2[j],NULL);
		j++;
	}
	
	zip();
	remove_file();
	unzip2();
	
	exit(0);
	
	return 0;
}
