# soal-shift-sisop-modul-3-F08-2022
**<br>1. Reza Maranelo Alifiansyah (5025201071)**
**<br>2. Natya Madya Marciola (5025201238)**
**<br>3. Khuria Khusna (5025201053)**

## Soal 1
Novak adalah seorang mahasiswa biasa yang terlalu sering berselancar di internet. Pada suatu saat, Ia menemukan sebuah informasi bahwa ada suatu situs yang tidak memiliki pengguna. Ia mendownload berbagai informasi yang ada dari situs tersebut dan menemukan sebuah file dengan tulisan yang tidak jelas. Setelah diperhatikan lagi, kode tersebut berformat base64. Ia lalu meminta kepada anda untuk membuat program untuk memecahkan kode-kode di dalam file yang Ia simpan di drive dengan cara decoding dengan base 64. Agar lebih cepat, Ia sarankan untuk menggunakan thread.

a. Download dua file zip dan unzip file zip tersebut di dua folder yang berbeda dengan nama quote untuk file zip quote.zip dan music untuk file zip music.zip. Unzip ini dilakukan dengan bersamaan menggunakan thread.

b. Decode semua file .txt yang ada dengan base 64 dan masukkan hasilnya dalam satu file .txt yang baru untuk masing-masing folder (Hasilnya nanti ada dua file .txt) pada saat yang sama dengan menggunakan thread dan dengan nama quote.txt dan music.txt. Masing-masing kalimat dipisahkan dengan newline/enter.

c. Pindahkan kedua file .txt yang berisi hasil decoding ke folder yang baru bernama hasil.

d. Folder hasil di-zip menjadi file hasil.zip dengan password 'mihinomenest[Nama user]'. (contoh password : mihinomenestnovak)

e. Karena ada yang kurang, kalian diminta untuk unzip file hasil.zip dan buat file no.txt dengan tulisan 'No' pada saat yang bersamaan, lalu zip kedua file hasil dan file no.txt menjadi hasil.zip, dengan password yang sama seperti sebelumnya.

### Penyelesaian Soal
> ***Note :*** Semua kode yang tertera merupakan potongan kode dan kemungkinan tidak berurutan

**Nomor 1a**
<br>Untuk mengerjakan soal no 1a, kita bisa membuat sebuah fungsi `download` sebagai berikut
```ruby
void download() {
	pid_t child_id;
 	int status;

  char *url[] = {
    "https://drive.google.com/uc?export=download&id=1jR67_JAyozZPs2oYtYqEZxeBLrs-k3dt",
    "https://drive.google.com/uc?export=download&id=1_djk0z-cx8bgISFsMiUaXzty2cT4VZp1"
  	};
	char *file[] = {"quote.zip", "music.zip"};
	
  for(int i = 0; i < 2; i++) {
  char *argv[] = {"wget", "-q", "--no-check-certificate", url[i], "-O", file[i], NULL};
    if((child_id = fork()) == 0) {
      execv("/usr/bin/wget", argv);
    }

    while(wait(&status) > 0);
	}
}
```
Kemudian setelah didownload, kita juga bisa membuat fungsi untuk meng-unzip file yang sudah kita download tadi sebagai berikut
```ruby
void* unzip(void *arg)
{
	int status;
	char *argv1[] = {"mkdir", "-p", "/home/natya/Modul3/quote", NULL};
	char *argv2[] = {"mkdir", "-p", "/home/natya/Modul3/music", NULL};
	char *argv3[] = {"unzip","-q" ,"quote.zip","-d","/home/natya/Modul3/quote", NULL};
	char *argv4[] = {"unzip","-q" ,"music.zip","-d","/home/natya/Modul3/music", NULL};
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid[0]))
	{
		child = fork();
		if (child==0) {
		    execv("/usr/bin/mkdir", argv1);
	    	}
	   while(wait(&status) > 0);
	}
	else if(pthread_equal(id,tid[1])) 
	{
       child = fork();
        if (child==0) {
		    execv("/usr/bin/mkdir", argv2);
	    }
	   while(wait(&status) > 0);
	}
	else if(pthread_equal(id,tid[2])) 
	{
       child = fork();
        if (child==0) {
		    execv("/usr/bin/unzip", argv3);
	    }
	   while(wait(&status) > 0);
	}
	else if(pthread_equal(id,tid[3])) 
	{
       child = fork();
        if (child==0) {
		    execv("/usr/bin/unzip", argv4);
	    }
	    while(wait(&status) > 0);
	}
	
	return NULL;
}
```
Untuk menjalankan fungsi-fungsi tersebut, kita bisa mengetikkan ini pada fungsi main
```ruby
int i=0, j=0;
int err,err2,err3;

download();
	
	//unzip
	while(i < 4) // loop sejumlah thread
	{
		err=pthread_create(&(tid[i]),NULL,&unzip,NULL); //membuat thread
		i++;
	}
	
	while(j < 4){
		pthread_join(tid[j],NULL);
		j++;
	}
	
```
> Thread ini berfungsi agar proses tersebut berjalan secara bersamaan

**Nomor 1b dan 1c**
<br>Untuk men-decode file yang ada dengan base 64, di sini kita juga bisa membuat sebuah fungsi sebagai berikut
```ruby
char base46[] = {'A', 'B', 'C', 'D', 'E', 'F', 'G', 'H', 'I', 'J', 'K', 'L', 'M', 'N', 'O', 'P', 'Q', 'R', 'S', 'T', 'U', 'V', 'W', 'X', 'Y', 'Z', 'a', 'b', 'c', 'd', 'e', 'f',
                     'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', '+', '/'};

char* base64_decode(char* c) {

    char counts = 0;
    char buffer[4];
    char* plain = malloc(strlen(c) * 3 / 4);
    int i = 0, p = 0;

    for(i = 0; c[i] != '\0'; i++) {
        char k;
        for(k = 0 ; k < 64 && base46[k] != c[i]; k++);
        buffer[counts++] = k;
        if(counts == 4) {
            plain[p++] = (buffer[0] << 2) + (buffer[1] >> 4);
            if(buffer[2] != 64)
                plain[p++] = (buffer[1] << 4) + (buffer[2] >> 2);
            if(buffer[3] != 64)
                plain[p++] = (buffer[2] << 6) + buffer[3];
            counts = 0;
        }
    }

    plain[p] = '\0';
    return plain;
}
```
Karena file hasil decode base 64 akan diletakkan pada folder baru bernama `hasil`, maka kita bisa membuat fungai untuk membuat folder baru
```ruby
void makedir(char *path){
	int status;
	pid_t child_id = fork();
	if (child_id == 0) {
		char *argv[] = {"mkdir", "-p", path, NULL};
		execv("/usr/bin/mkdir", argv);
	} else {
		((wait(&status)) > 0);
	}
}
```
Lalu untuk meletakkan file-file tersebut, terdapat dua fungsi yang akan berperan, yaitu : <br>
`Fungsi 1`
```ruby
void listFilesRecursively(char *basePath, char *hasilPath)
{
    char path[100];
    struct dirent *dp;
    DIR *dir = opendir(basePath);

    if (!dir)
        return;

    while ((dp = readdir(dir)) != NULL)
    {
            strcpy(path, basePath);
            strcat(path, "/");
            strcat(path, dp->d_name);

 				FILE *fp = fopen(path, "r");
 				
 				const unsigned MAX_LENGTH = 256;
    			char buffer[MAX_LENGTH];

    			while (fgets(buffer, MAX_LENGTH, fp)){
    				FILE *hasil = fopen(hasilPath,"a");
    				fprintf(hasil,"%s\n", base64_decode(buffer));
    				fclose(hasil);
    			}
        		
				 // close the file
				 fclose(fp);
       
    }

    closedir(dir);
}
```
> Fungsi ini akan membuka file satu persatu secara rekursif, kemudian memasukkan hasil dari decode tersebut ke dalam file hasil.txt

`Fungsi 2`
```ruby
void* decode(void *arg){
	pthread_t id=pthread_self();
	if(pthread_equal(id,tid2[0]))
	{
		listFilesRecursively("/home/natya/Modul3/music", "/home/natya/Modul3/hasil/music.txt");
	}
	else if(pthread_equal(id,tid2[1])) 
	{
       listFilesRecursively("/home/natya/Modul3/quote", "/home/natya/Modul3/hasil/quote.txt");
	}
}
```
> Fungsi ini memanfaatkan thread untuk melakukan decode pada masing-masing folder secara bersamaan

Untuk menjalankannya pada fungsi main, akan diketikkan sebagai berikut
```ruby
makedir("/home/natya/Modul3/hasil");
	//decode
	while(i < 3) // loop sejumlah thread
	{
		err2=pthread_create(&(tid2[i]),NULL,&decode,NULL);
		i++;
	}
	
	while(j < 3){
		pthread_join(tid2[j],NULL);
		j++;
	}
```

**Nomor 1d**
<br>Untuk melakukan zip file dengan password, kita bisa mmebuat sebuah fungsi baru
```ruby
void zip(){
	int status;
	char *argv[] = {"zip", "-P", "mihinomenestnatya", "-qr", "hasil.zip", "hasil", NULL};
	child = fork();
		if (child==0) {
		    execv("/usr/bin/zip", argv);
	    	}
	   while(wait(&status) > 0);
}
```
Dan setelah file tersebut di-zip, kita akan menghapus folder tadi dengan fungsi sebagai berikut
```ruby
void remove_file(){
	int status;
	char *argv[] = {"rm", "-r", "/home/natya/Modul3/hasil", NULL};
	child = fork();
	if (child==0) {
		execv("/usr/bin/rm", argv);
	}
	while(wait(&status) > 0);
}
```
Lalu kita hanya perlu mengetikkan berikut pada fungsi main untuk menjalankan fungsi tersebut
```ruby
zip();
remove_file();
```

**Nomor 1e**
<br>Pada soal, diminta untuk meng-unzip folder tadi. Namun, unzip pada nomor 1e ini perlu untuk memasukkan password yang kita gunakan pada saat zip file tadi, sehingga kita perlu membuat fungsi khusus untuk meng-unzipnya
```
void unzip2(){
	   
	int status;
	char *argv[] = {"unzip", "-P", "mihinomenestnatya", "-q", "/home/natya/Modul3/hasil.zip", NULL};

	child = fork();
	if (child==0) {
	    execv("/usr/bin/unzip", argv);
	}
	while(wait(&status) > 0);
	
}
```
Lalu pada fungsi main, kita hanya perlu mengetikkan sebagai berikut
```ruby
unzip2();
```

Contoh hasil setelah program dijalankan
![error1](img/1.png)
![error1](img/2.png)
![error1](img/3.png)
![error1](img/4.png)

## Kendala
- Masih rada bingung dalam penggunaan thread
